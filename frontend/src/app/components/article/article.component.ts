import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../../services/article.service';
import { Article } from 'src/app/models/article';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Global } from '../../services/global';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css'],
  providers: [ArticleService]
})
export class ArticleComponent implements OnInit {

  public article: Article;
  public url: string;

  constructor(private _articleService: ArticleService,
    private _route: ActivatedRoute,
    private _router: Router) { this.url = Global.url; }

  ngOnInit(): void {

    this._route.params.subscribe(params => {
      let id = params['id'];
      this._articleService.getArticle(id).subscribe(
        response => {
          //console.log(response);
          if (response.article) {
            this.article = response.article;
          } else {
            this._router.navigate(['/home']);
          }
        }, error => {
          console.log(error);
          this._router.navigate(['/home']);
        }
      );
    });

  }


  delete(id) {
    Swal.fire({
      title: "¿Estas Seguro?",
      text: "Una vez borrado el articulo no podras recuperarlo!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar Articulo'
    })
    .then((result) => {
      if (result.value) {
        this._articleService.delete(id).subscribe(
          response => {
            Swal.fire(
              'Eliminado!',
              'El articulo ha sido eliminado',
              'success'
            )
            this._router.navigate(['/blog'])
            //console.log(response)
          },
          error => {
            console.log(error)
            this._router.navigate(['/blog'])
          }
        );
        } else {
          Swal.fire('Tranquilo, El articulo no se ha eliminado =)')
        }
      });
  }

}
