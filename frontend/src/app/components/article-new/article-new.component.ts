import { Component, OnInit } from '@angular/core';
import { Article } from '../../models/article';
import { ArticleService } from '../../services/article.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Global } from '../../services/global';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-article-new',
  templateUrl: './article-new.component.html',
  styleUrls: ['./article-new.component.css'],
  providers: [ArticleService]
})
export class ArticleNewComponent implements OnInit {

  public article: Article;
  public status: string;
  public page_title: string;
  public is_Edit: Boolean;
  public url: string;

  afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.png, .gif, .jpeg",
    maxSize: "50",
    uploadAPI: {
      url: Global.url + '/upload-image/'
      /*headers: {
          "Content-Type" : "text/plain;charset=UTF-8",
          "Authorization" : `Bearer ${token}`
           }*/

    },
    theme: "attachPin",
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: false,
    replaceTexts: {
      selectFileBtn: 'Select Files',
      resetBtn: 'Reset',
      uploadBtn: 'Upload',
      dragNDropBox: 'Drag N Drop',
      attachPinBtn: 'Sube la Imagen para el articulo',
      afterUploadMsg_success: 'Successfully Uploaded !',
      afterUploadMsg_error: 'Upload Failed !'
    }
  };


  constructor(private _articleService: ArticleService,
    private _route: ActivatedRoute,
    private _router: Router, ) {
    this.article = new Article('', '', '', null, null);
    this.page_title = 'Crear Articulo';
    this.is_Edit = true;
    this.url = Global.url;
  }

  ngOnInit(): void {
  }

  onSubmit() {
    //console.log(this.article);
    this._articleService.create(this.article).subscribe(
      response => {
        if (response.status == 'success') {
          this.status = 'success';
          this.article = response.article;
          
          /*Swal(
            'Articulo Creado !!',
            'El articulo se ha creado correctamente',
            'success'
          );*/

          Swal.fire(
            'Articulo Creado !!',
            'El articulo se ha creado correctamente',
            'success'
          );

          /*Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Your work has been saved',
            showConfirmButton: false,
            timer: 1500
          })*/

          //console.log(response);
          this._router.navigate(['/blog']);
        } else {
          this.status = 'error'
        }
      },
      error => {
        console.log(error);
        this.status = 'error'
      }
    );
  }

  imageUpload(data) {
    let image_data = JSON.parse(data.response);
    //alert(image_data.image);
    this.article.image = image_data.image;
  }

}
