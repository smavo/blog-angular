import { Component, OnInit } from '@angular/core';
import { Article } from '../../models/article';
import { ArticleService } from '../../services/article.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Global } from '../../services/global';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-article-edit',
  templateUrl: '../article-new/article-new.component.html',
  styleUrls: ['./article-edit.component.css'],
  providers: [ArticleService]
})
export class ArticleEditComponent implements OnInit {

  public article: Article;
  public status: string;
  public page_title: string;
  public is_Edit: Boolean;
  public url: string;

  afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.png, .gif, .jpeg",
    maxSize: "50",
    uploadAPI: {
      url: Global.url + '/upload-image/'
      /*headers: {
          "Content-Type" : "text/plain;charset=UTF-8",
          "Authorization" : `Bearer ${token}`
           }*/

    },
    theme: "attachPin",
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: false,
    replaceTexts: {
      selectFileBtn: 'Select Files',
      resetBtn: 'Reset',
      uploadBtn: 'Upload',
      dragNDropBox: 'Drag N Drop',
      attachPinBtn: 'Sube la Imagen para actualizar el articulo',
      afterUploadMsg_success: 'Successfully Uploaded !',
      afterUploadMsg_error: 'Upload Failed !'
    }
  };


  constructor(private _articleService: ArticleService,
    private _route: ActivatedRoute,
    private _router: Router, ) {
    this.article = new Article('', '', '', null, null);
    this.page_title = 'Editar Articulo';
    this.is_Edit = true;
    this.url = Global.url;
  }

  ngOnInit(): void {
    this.getArticle();
  }

  onSubmit() {
    //console.log(this.article);
    this._articleService.update(this.article._id,this.article).subscribe(
      response => {
        if (response.status == 'success') {
          this.status = 'success';
          this.article = response.article;
          //console.log(response);
          
          
          //Alerta 
          Swal.fire(
            'Articulo Actualizado !!',
            'El articulo se ha actualizado correctamente',
            'success'
          );

          this._router.navigate(['/blog/articulo', this.article._id]);
        } else {
          this.status = 'error';
        }
      },
      error => {
        console.log(error);
        this.status = 'error';
        Swal.fire(
          'Actualizacion fallida',
          'El articulo no se ha actualizado',
          'error'
        );
        
      }
    );
  }

  imageUpload(data) {
    let image_data = JSON.parse(data.response);
    //alert(image_data.image);
    this.article.image = image_data.image;
  }

  getArticle() {
    this._route.params.subscribe(params => {
      let id = params['id'];
      this._articleService.getArticle(id).subscribe(
        response => {
          //console.log(response);
          if (response.article) {
            this.article = response.article;
          } else {
            this._router.navigate(['/home']);
          }
        }, error => {
          console.log(error);
          this._router.navigate(['/home']);
        }
      );
    });
  }

}
