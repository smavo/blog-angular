import { ModuleWithProviders, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//Importando Componentes
import { HomeComponent} from './components/home/home.component';
import { BlogComponent } from './components/blog/blog.component';
import { AngularComponent } from './components/angular/angular.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { Error404Component } from './components/error404/error404.component';
import { ArticleComponent } from './components/article/article.component';
import { SearchComponent } from './components/search/search.component';
import { ArticleNewComponent } from './components/article-new/article-new.component';
import { ArticleEditComponent } from './components/article-edit/article-edit.component';



const app_routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'blog', component: BlogComponent },
    { path: 'angular', component: AngularComponent },
    { path: 'blog/articulo/:id', component: ArticleComponent },
    { path: 'blog/new', component: ArticleNewComponent },
    { path: 'blog/edit/:id', component: ArticleEditComponent },
    { path: 'formulario', component: FormularioComponent },
    { path: 'buscar/:search', component: SearchComponent },
    { path: '**', component: Error404Component } 
    /*{ path: '**', pathMatch: 'full', redirectTo: 'home' }*/
];

export const appRoutingProviders: any[] = [];
export const app_routing: ModuleWithProviders = RouterModule.forRoot(app_routes);
